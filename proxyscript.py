#This python script will see if there is a .pdf saved, if yes then it will print "Got an intriguing flow"
import mitmproxy

def request(flow):
    #code to handle request flows
    if flow.request.pretty_url.endswith(".pdf"):
        print("[+] Got an intriguing flow")
        print(flow)

