# This python script will print "No soup for you!" on the webpage where the .pdf file is opened
import mitmproxy

def request(flow: mitmproxy.http.HTTPFlow) -> None:
    #code to handle request flows
    if flow.request.pretty_url.endswith("pdf"):
        print("[+] Got an intriguing flow")
        flow.response = mitmproxy.http.HTTPResponse.make(
                200,
                b"No soup for you!",
                {"Content-Type": "text/html"}) # (optional headers)

